What?
=====
Language-learning games written in Racket.

For M.

License
=======

For now, [Creative Commons Attribution Share-Alike 4.0][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
